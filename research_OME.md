Research on the OME education :
========================================

What is the learning goals of the education?
------------

To attain knowledge and skills needed to be responsible for management 
and maintenance of technical facilities and installations both on- and offshore.

What educational level in the qualification framework is the OME education?
------------

The OME education is placed at educational level 6 out of 8 total levels.

This means that students attain knowledge of theory, methodologies
and practice within a given field, as well as being able to understand and reflect on these.

What courses are included in the education?
------------

The education includes courses in Methodology, thermic machinery 
and installations, electrical and electronic machinery, installations 
and equipment, automation as well as management, finance and security.

What kind of jobs does OME's occupy?
------------

OMEs are hired for Project Management, Sales Engineering, Environment-
and Quality control manager, technical manager, advisory positions regarding energy.

What kind of documentation models does OME's use?
------------

What challenges can we experience in our communication with OME's
------------

Having different understanding or angles of various elements in a project, 
realizing when such situations occur and how to get a common understanding 
where all members can agree on a relevant solution within narrow time constraints.

Other relevant questions that you would like knowledge about
------------
